# README

## Setup
    create a virtual environment `-m venv env`
    start virtial environment 'source env/bin/activate
    install packages from requirements.txt `pip3 install -r requirements.txt`

## Important notes
    * If this project is not in a private repo, make sure to take out the passwords in settings and fileconversions.py before pushing to repo
    * Make sure the machine you are running this on is listed in ALLOWED_HOSTS in settings
    * If you make changes to settings, make sure to you are using the right database before putting live. You also need to change 'cocodatiserver' to 'cocodati_server' in settings.py before putting it live.
    * The following files hafe global variables defined high up in their file.  You need to make sure they have the right values before putting them live:
        - fileconversions.py
        - postfile.py

## fileconversions.py
    This file runs as a cronjob every minute.

## Development
    run python3 `manage.py runserver 0.0.0.0:8088`
    The client is expecting the server to be run on port 8088, you can of course change this as long as you change it on both sides.
    
## Production
    Live code is on sandmerki at `\\sandmerki\lmiop\cocodati_server`