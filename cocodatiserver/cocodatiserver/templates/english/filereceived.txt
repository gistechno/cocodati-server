Dear $username,

Thank you for using Cocodati.  We have received your file $filename.

We will email you again as soon as we have finished converting your file from $reffrom $coordfrom to $refto $coordto.

Your file key is $id

Best regards,
The National Land Survey of Iceland