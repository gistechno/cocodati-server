Kæri/a $username,

Þakka þér fyrir að nota Cocodati.  Við höfum móttekið skrána þína, $filename.

Því miður hefur komið upp villa við að umbreyta hnitunum þínum.

Gott væri að þú myndir athuga skrána þína og sjá hvort eitthvað í henni gæti verið vitlaust.

Þú getur reynt aftur þegar búið er að laga skrána.  Ef þú telur að skráin sé rétt þá getur þú haft samband við okkur á lmi (hjá) lmi.is, mundu að senda skráarlykilinn með.

Skráarlykillinn þinn er $id

Með bestu kveðju,
Landmælingar Íslands