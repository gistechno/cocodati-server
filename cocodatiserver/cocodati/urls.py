#! /usr/bin/python3
# -*- coding: utf-8 -*-
from django.urls import path, include #Django 2.0
from rest_framework import routers
from . import views, conversions, configs, postfile
from django.conf import settings

app_name = 'cocodati-server'
router = routers.DefaultRouter()
router.register(r'config', configs.getConfigViewSet, basename="config")
router.register(r'convert',conversions.convertViewSet, basename="convert")
router.register(r'convertfile',postfile.fileConvertViewset, basename="fileconvert")
urlpatterns = [
    path('cocodati-server', conversions.index, name='index'),
    path('cocodati-server/api/', include(router.urls))
]