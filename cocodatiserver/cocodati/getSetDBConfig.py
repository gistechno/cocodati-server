#! /usr/bin/python3
# -*- coding: utf-8 -*-
#
import os
import json
from django.conf import settings
from django import forms
import re
from subprocess import Popen,PIPE
import subprocess
import uuid
import ast
from datetime import datetime
from string import Template
import psycopg2
from django.db import connection
import fileinput

settings.configure(
    DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.postgresql_psycopg2',
        #'NAME': 'cocodati',
        #'USER': 'cocodati',
        #'PASSWORD': '',
        #'HOST': 'akrar3.lmi.is',
        #'PORT': '5432',
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cocodati',
        'USER': 'bva',
        'PASSWORD': '',
        'HOST': 'akrar-test.lmi.is',
        'PORT': '5432',
       
    }
    },
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
)
global _file_location
_file_location = os.path.abspath(os.path.dirname(__file__))

def writeToDB():
    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    with open(os.path.join(t, 'configs/config.json')) as f:
                data = json.load(f)

    _configs = data['systems']

    counter = 0


    with connection.cursor() as cursor:
        update_log_stri = 'INSERT INTO conversion_configs(system_from, height_from,sub_from,system_to,height_to,sub_to,command,height,zone) VALUES (%s, %s, %s,%s,%s,%s,%s,%s,%s);' 
                            
        for systems_from in _configs:
            for heights_from in _configs[systems_from]:
                for sub_from in _configs[systems_from][heights_from]:
                    for sys_to in _configs[systems_from][heights_from][sub_from]:
                        for h_to in _configs[systems_from][heights_from][sub_from][sys_to]:
                            for sub_to in _configs[systems_from][heights_from][sub_from][sys_to][h_to]:
                                counter += 1
                                fields = _configs[systems_from][heights_from][sub_from][sys_to][h_to][sub_to]
                                
                                command = ' '.join(fields['command'])
                                zone = fields['zone']
                                height = fields['height']
                                print(command)
                                log_data = (systems_from,heights_from,sub_from,sys_to,h_to,sub_to,command,height,zone)

                                cursor.execute(update_log_stri, log_data)

    print("Number of configs: " + str(counter) )



def writeToTestCases():
    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    with open(os.path.join(t, 'configs/testcases_test.json')) as f:
                data = json.load(f)

    _cases = data['cases']

    counter = 0
    with connection.cursor() as cursor:
        update_log_stri = 'INSERT INTO config_test_cases(system_,system_to,to_,from_,y,x,z,zone_,expected_y,expected_x,expected_z,latlong_format,height_from,height_to,from_epoch,to_epoch,gk_zone) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s, %s, %s,%s,%s,%s,%s,%s,%s);'
        for i in _cases:
            counter += 1
            try:
                from_epoch = i['from_epoch']
                to_epoch = i['to_epoch']   
            except:
                from_epoch = ""
                to_epoch = ""

            try:
                gk_zone = i['gk_zone'] 
            except:
                gk_zone = ""

            to = i['to']   
                            
            log_data = (i['system'],i['system_to'],to,i['from'],i['y'],i['x'],i['z'],i['zone'],i['expected_Y'],i['expected_X'],i['expected_Z'],i['latlong_format'],i['height_from'],i['height_to'],from_epoch,to_epoch,gk_zone)
            
            cursor.execute(update_log_stri, log_data)
    

    print(counter)

def readFromDB():
    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'
    with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM conversion_configs;')
            res = cursor.fetchall()
    
    for i in res:
        print(i)

#writeToDB()
#readFromDB()