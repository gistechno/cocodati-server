#! /usr/bin/python3
# -*- coding: utf-8 -*-
#
import os
from django.conf import settings
settings.configure(
    DATABASES = {
    'default': {
       
    }
    },
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
)
import json
from django import forms
import re
from subprocess import Popen,PIPE
import subprocess
import uuid
import ast
from datetime import datetime
from string import Template
import psycopg2
from django.db import connection
import fileinput

global _location 
#TODO uncomment
_location = '/var/www/sandmerki/html/cocodati-files/'
#_location = '/var/www/jardbakki/html/cocodati/'

global _file_location
_file_location = os.path.abspath(os.path.dirname(__file__))

def main():
    fileconvert()

def sendFailedEmail(_id,_filename, _username, _email, _language):
    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    with open(os.path.join(t, 'templates/'+ _language + '/fileerror.txt')) as f, open(_location + str(_id) + '/out/sendfile.txt', 'w') as fconv:
        src = Template(f.read())
        d = {'username':_username, 'id':_id, 'filename':_filename}
        result = src.substitute(d)
        fconv.write(result)

    _input = ['/bin/cat', _location + str(_id) + '/out/sendfile.txt']
    _command = [_file_location + '/send-mail.sh', '-f', 'lmi@lmi.is', '-s', 'Cocodati Conversions', '-v', _email]

    p1 = Popen(_input, stdout=PIPE)
    p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    p2.communicate()[0]


def sendEmail(_id,_filename, _username, _email, _language):
    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    #TODO uncomment
    #templink = '/cocodati-files/' + str(_id) + '/out/' + _filename
    templink = 'https://sandmerki.lmi.is/cocodati-files/' + str(_id) + '/out/' + _filename 

    with open(os.path.join(t, 'templates/'+ _language + '/sendfile.txt')) as f, open(_location + str(_id) + '/out/sendfile.txt', 'w') as fconv:
        src = Template(f.read())  
        d = {'username':_username, 'link': templink }
        result = src.substitute(d)
        fconv.write(result)

    temp_location = _location + str(_id) + '/out/' + _filename

    _input = ['/bin/cat', _location + str(_id) + '/out/sendfile.txt']
    if(os.path.getsize(temp_location) < 20000000):
        _command = [_file_location + '/send-mail.sh', '-f', 'lmi@lmi.is', '-A', _location + str(_id) + '/out/' + _filename, '-s', 'Cocodati Conversions', '-v', _email]
    else:
        _command = [_file_location + '/send-mail.sh', '-f', 'lmi@lmi.is', '-s', 'Cocodati Conversions', '-v', _email]

    p1 = Popen(_input, stdout=PIPE)
    p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    p2.communicate()[0]

    _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    update_status_stri = 'UPDATE fileconversions SET status = 3, updated = %s WHERE id = %s;'
    status_data = (_updated,str(_id))
    update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed) VALUES (%s, 3, %s);' 
    log_data = (str(_id),_updated)
    
    with connection.cursor() as cursor:
        cursor.execute(update_status_stri, status_data)
        cursor.execute(update_log_stri, log_data)

def formatLatLong(_id, _latlong_format, _file_format, _is_only_dms_convert):
    conversion_results_file = '/out/conversionresults'

    with open(_location + str(_id) + conversion_results_file + _file_format, 'r') as f, open(_location + str(_id) + '/out/latlongformatter' + _file_format, 'w') as llf:
        for line in f:
            t = line.split()
            if len(t) != 0:
                xd = t[0][:t[0].find('.')]
                yd = t[1][:t[1].find('.')]
                
                xm = '0' + t[0][t[0].find('.'):]
                xm = float(xm) * 60
                xm = str(xm)

                ym = '0' + t[1][t[1].find('.'):]
                ym = float(ym) * 60
                ym = str(ym)
                if _latlong_format == 'DM':
                    if _is_only_dms_convert:
                        temp = [str(xd) ,str(xm), str(yd), str(ym), t[2],'\n']
                    else:
                        temp = [str(yd), str(ym),str(xd) ,str(xm), t[2],'\n']
                else:
                    xs = xm
                    xs = xs[xs.find('.'):]
                    xs = '0' + xs
                    xs = float(xs) * 60
                    ys = ym
                    ys = ys[ys.find('.'):]
                    ys = '0' + ys
                    ys = float(ys) * 60
                    xm = xm[:xm.find('.')]
                    ym = ym[:ym.find('.')]
                    if _is_only_dms_convert:
                        ys = round(ys,4)
                        xs = round(xs,4)
                        temp = [str(xd) ,str(xm), str(xs),str(yd), str(ym), str(ys),  t[2],'\n']
                    else:
                        temp = [str(yd), str(ym), str(ys), str(xd) ,str(xm), str(xs), t[2],'\n']
            
                llf.write("\t".join(str(x) for x in temp))
            else:
                llf.write(line)

def utmHandler(_zone, _command):
    if _zone == '0':
        print("Error? No UTM zone given")
    else:
        if '+init=IS.txt:ISN_UTM28' in _command:
            utmIndex = _command.index('+init=IS.txt:ISN_UTM28')
            _command[utmIndex] = '+init=IS.txt:ISN_UTM' + _zone
            if '+init=IS.txt:ISN_UTM28' in _command:
                utmIndex2 = _command.index('+init=IS.txt:ISN_UTM28')
                if(utmIndex2 +1):
                    _command[utmIndex2] = '+init=IS.txt:ISN_UTM' + _zone
        
        if '+init=IS.txt:HJ_UTM28' in _command:
            hjorseyIndex = _command.index('+init=IS.txt:HJ_UTM28')
            if(hjorseyIndex + 1):
                _command[hjorseyIndex] = ("+init=IS.txt:HJ_UTM" + _zone)
            if '+init=IS.txt:HJ_UTM28' in _command:
                hjorseyIndex = _command.index('+init=IS.txt:HJ_UTM28')
                if(hjorseyIndex + 1):
                    _command[hjorseyIndex] = ("+init=IS.txt:HJ_UTM" + _zone)
    return _command

def gkHandler(_gkZone, _command):
    if "+init=IS.txt:ISN_GK18" in _command:
        gkIndex = _command.index("+init=IS.txt:ISN_GK18")
        _command[gkIndex] = ("+init=IS.txt:ISN_GK" + _gkZone)
        if "+init=IS.txt:ISN_GK18" in _command:
            gkIndex2 = _command.index("+init=IS.txt:ISN_GK18")
            if(gkIndex2 + 1):
                _command[gkIndex2] = ("+init=IS.txt:ISN_GK" + _gkZone)

    if '+init=IS.txt:HJ_GK18' in _command:
        hjorseyIndex = _command.index('+init=IS.txt:HJ_GK18')
        if(hjorseyIndex + 1):
            _command[hjorseyIndex] = ("+init=IS.txt:HJ_GK" + _gkZone)
        if '+init=IS.txt:HJ_GK18' in _command:
            hjorseyIndex = _command.index('+init=IS.txt:HJ_GK18')
            if(hjorseyIndex + 1):
                _command[hjorseyIndex] = ("+init=IS.txt:HJ_GK" + _gkZone)
    return _command

def latlongHelper(_id, _filename, _file_format, _file_header, _has_name,_is_only_dms_convert):
    if(_is_only_dms_convert == 1):
        _file_out = '/out/conversionresults'
        _filename = 'out/latlonghelper.txt'
    else:
        _file_out = '/out/latlonghelper'
    with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + _file_out + _file_format, 'w') as fconv:
        for i, line in enumerate(fin):
            if i >= int(_file_header) and line[0] == '%':
                _file_header += 1
            elif i >= int(_file_header) and line[0] != '%':
                _check_format = line.split()
                
                if(len(_check_format) < 5) or _is_only_dms_convert:
                    #format is 65.5
                    if(len(_check_format) == 3):
                        _has_name = False

                    _check_format.append('\n')
                    if 1 == _is_only_dms_convert:
                        if len(_check_format) == 6 or (len(_check_format) == 5 and _check_format[-2] != '0') :
                            fconv.write("\t".join(str(x) for x in _check_format[1:]))
                        else:
                            fconv.write("\t".join(str(x) for x in _check_format))
                    else:
                        fconv.write("\t".join(str(x) for x in _check_format))
                elif(len(_check_format) < 7):
                    #format is 65 50
                    
                    if(len(_check_format) == 6):
                        t1 = _check_format[0]
                        
                        
                        mp = 1
                        if(float(_check_format[1]) < 0):
                            mp = -1
                        t2 = (float(_check_format[1]) + (mp * (float(_check_format[2])/60)))

                        if(float(_check_format[3]) < 0):
                            mp = -1
                        t3 = (float(_check_format[3]) + (mp * (float(_check_format[4])/60)))
                        
                        t = [t1, str(t2) ,str(t3), _check_format[5]]

                        if _is_only_dms_convert != 1:
                            t = [t1, str(t2) ,str(t3), _check_format[5]]
                            t = t + ['0','\n']
                        
                        #t = [t1, str(t2) ,str(t3), _check_format[5], '0','\n']
                        
                        fconv.write("\t".join(str(x) for x in t))
                    else: #No name for coordinates
                        _has_name = False
                        mp = 1
                        if(float(_check_format[0]) < 0):
                            mp = -1
                        t2 = (float(_check_format[0]) + (mp * (float(_check_format[1])/60)))

                        if(float(_check_format[2]) < 0):
                            mp = -1
                        t3 = (float(_check_format[2]) + (mp * (float(_check_format[3])/60)))
                        
                        t = [str(t2) ,str(t3), _check_format[4]]

                        if _is_only_dms_convert != 1:
                            t = t + ['0','\n']
                        
                        #t = [str(t2) ,str(t3), _check_format[4], '0','\n']

                        
                        
                        
                        fconv.write("\t".join(str(x) for x in t))

                elif(len(_check_format) < 9):
                    #format is 65 50 0
                    if(len(_check_format) == 8):
                        t1 = _check_format[0]

                        mp = 1
                        if(float(_check_format[1]) < 0):
                            mp = -1
                        t2 = (float(_check_format[1]) + (mp * (float(_check_format[2])/60)) + (mp * (float(_check_format[3])/3600)))

                        if(float(_check_format[4]) < 0):
                            mp = -1
                        t3 = (float(_check_format[4]) + (mp * (float(_check_format[5])/60)) + (mp * (float(_check_format[6])/3600)))
                        t = [t1, str(t2) ,str(t3), _check_format[7]]

                        if _is_only_dms_convert != 1:
                            t = t + ['\n']

                        fconv.write("\t".join(str(x) for x in t))
                    else: #No name for coordinates
                        _has_name = False
                        mp = 1
                        if(float(_check_format[0]) < 0):
                            mp = -1
                        t2 = (float(_check_format[0]) + (mp * (float(_check_format[1])/60)) + (mp * (float(_check_format[2])/3600)))

                        if(float(_check_format[3]) < 0):
                            mp = -1
                        t3 = (float(_check_format[3]) + (mp * (float(_check_format[4])/60)) + (mp * (float(_check_format[5])/3600)))
                        t = [str(t2) ,str(t3), _check_format[6]]

                        if _is_only_dms_convert != 1:
                            t = t + ['\n']
                        fconv.write("\t".join(str(x) for x in t))
        return _has_name

def hjorseyHelper( _id, _filename, _ref_frame_from, _coorsystem_from, _file_header, _utm_zone, _gk_zone, _file_format, _height_from, _has_name):
    #Convert to ISN93 LCC
    #Convert from ISN93 LCC to correct reference frame
    skiplines = ["-s",str(_file_header)]
    #If input file has a name for each coordinate, columns 2,3,4,5 are used for the input
    # + make sure we use helper file to keep input-ed coordinates on correct format for cct
    if(_has_name):
        if(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
            columns = ["-c","3,2,4,5"]
            _input_file = 'out/latlonghelper' + _file_format
            skiplines = ["-s",'0']
        else:
            columns = ["-c","2,3,4,5"]
            _input_file = _filename
    else:
        if(_coorsystem_from == 'LatLong'  or _coorsystem_from == 'LatLongCPH'):
            columns = ["-c","2,1,3,4"]
            _input_file = 'out/latlonghelper' + _file_format
            skiplines = ["-s",'0']
        else:
            columns = ["-c","1,2,3,4"]
            _input_file = _filename

    outputfile = ['-o', _location + str(_id) + '/out/ISN93LCC' + _file_format]

    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    with open(os.path.join(t, 'configs/config.json')) as f:
        _config = json.load(f)

    _command = _config['systems'][_ref_frame_from][_height_from][_coorsystem_from]['ISN93']['h']['LCC']['command']

    # If converting to or from UTM, we must append the correct UTM zone
    if _coorsystem_from == 'UTM':
        _command = utmHandler(_utm_zone, _command)

    # If converting to GK, we must find correct central meridian for GK (Transverse Mercator)
    if _coorsystem_from == 'GK':
        _command = gkHandler(_gk_zone, _command)

    
    _command = _command[0:1] + skiplines + columns + outputfile + _command[1:] + [_location + str(_id) + '/' + _input_file]

    p1 = Popen(_command, stdout=PIPE)
    p1.communicate()

def toEpochHandler(_command, _to_epoch):
    if "+t_epoch=2017.5" in _command:
        index = _command.index("+t_epoch=2017.5")
        _command[index] = ("+t_epoch=" + _to_epoch)
    elif "+t_epoch=2016.5" in _command:
        index = _command.index("+t_epoch=2016.5")
        _command[index] = ("+t_epoch=" + _to_epoch)

    return _command

def fromEpochHandler(_command, _from_epoch):
    if "+t_obs=2017.5" in _command:
        index = _command.index("+t_obs=2017.5")
        _command[index] = ("+t_obs=" + _from_epoch)
    elif "+t_obs=2016.5" in _command:
        index = _command.index("+t_obs=2016.5")
        _command[index] = ("+t_obs=" + _from_epoch)
    return _command

def epochHandler(_command, _from_epoch, _to_epoch):

    diff = float(_to_epoch) - float(_from_epoch)
    if "+t_epoch=2017.5" in _command:
        index = _command.index("+t_epoch=2017.5")
        _command[index] = ("+dt=" + str(diff))
    elif "+t_epoch=2016.5" in _command:
        index = _command.index("+t_epoch=2016.5")
        _command[index] = ("+dt=" + str(diff))

    if "+t_obs=2017.5" in _command:
        _command.remove("+t_obs=2017.5")
    elif "+t_obs=2016.5" in _command:
        _command.remove("+t_obs=2016.5")

    return _command

def drfISHHelper(_id, _filename, _ref_frame_from, _coorsystem_from, _file_header, _file_format, _height_from, _has_name):
    skiplines = ["-s",str(_file_header)]

    if(_has_name):
        if(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
            columns = ["-c","3,2,4,5"]
            _input_file = 'out/latlonghelper' + _file_format
            skiplines = ["-s",'0']
        else:
            columns = ["-c","2,3,4,5"]
            _input_file = _filename
    else:
        if(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
            columns = ["-c","2,1,3,4"]
            _input_file = 'out/latlonghelper' + _file_format
            skiplines = ["-s",'0']
        else:
            columns = ["-c","1,2,3,4"]
            _input_file = _filename

    #output = convert(_input, _command)

    #Get values on correct form from output string
    #temp = getValuesFromOutput(output, _from, _system, _systemTo)
    outputfile = ['-o', _location + str(_id) + '/out/DRF_ISH_Helper' + _file_format]

    t = _file_location
    t = t[:t.rfind('/')] + '/cocodatiserver'

    with open(os.path.join(t, 'configs/config.json')) as f:
        _config = json.load(f)

    
    _command = _config['systems'][_ref_frame_from][_height_from][_coorsystem_from]['ISN2016']['MSL']['LatLong']['command']
    
    _command = _command[0:1] + skiplines + columns + outputfile + _command[1:] + [_location + str(_id) + '/' + _input_file]

    p1 = Popen(_command, stdout=PIPE)
    p1.communicate()

def drfISHFormatter(_id,_coorsystem_to,_latlong_format,_filename, _file_format,_file_header,_has_name):
    if (_coorsystem_to == 'LatLong')  and _latlong_format != 'Decimal':
        with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + '/out/latlongformatter' + _file_format, 'r') as fconv:
            with open(_location + str(_id) + '/out/' + _filename, 'a') as fout, open(_location + str(_id) + '/out/DRF_ISH_Helper' + _file_format, 'r') as fheight:  
                for line in range(_file_header):
                    next(fin)
                for x, y,z in zip(fin,fconv, fheight):
                    x = x[:x.find('\t')]
                    
                    y = y[:y.rfind(' ')]
                    y = y.split()
                    y = y[:-1]
                    y = '\t'.join(y)

                    z = z.split()
                    z = z[2]
                    if(_has_name):
                        fout.write("{0}\t{1}\t{2}\n".format(x, y, z))
                    else:
                        fout.write("{0}\t{1}\n".format(y, z))
    else:
        #Append conversionresults to return file (same name as input file)
        with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + '/out/conversionresults' + _file_format, 'r') as fconv:
            with open(_location + str(_id) + '/out/' + _filename, 'a') as fout, open(_location + str(_id) + '/out/DRF_ISH_Helper' + _file_format, 'r') as fheight:  
                for line in range(_file_header):
                    next(fin)
                for x, y,z in zip(fin,fconv,fheight):
                    x = x[:x.find('\t')]

                    y = y[:y.rfind(' ')]
                    y = y.split()
                    y = y[:2]
                    y = '\t'.join(y)

                    z = z.split()
                    z = z[2]
                    #If to latlong, switch x and y values so latitude comes before longtitue
                    if(_coorsystem_to == 'LatLong'):
                        temp = y.split()
                        if(len(temp) > 1):
                            temp[1], temp[0] = temp[0],temp[1]
                        temp = temp + [z]
                        y = "   ".join(str(x) for x in temp)
                    if(_has_name):
                        fout.write("{0}\t{1}\t{2}\n".format(x, y ,z))
                    else:
                        fout.write("{0}\t{1}\n".format(y,z))
    

def fileconvert():
    try:
        with connection.cursor() as cursor:
            cursor.execute('SELECT id, filename, ref_frame_from, coorsystem_from, ref_frame_to, coorsystem_to, file_header, email, utm_zone, gk_zone, language, username, height_from, height_to, latlong_format, to_epoch, from_epoch FROM fileconversions WHERE status = 0 ORDER BY RECEIVED LIMIT 1;')
            res = cursor.fetchall()
            if(res):
                _data = res[0]
                _id = _data[0]
                _filename = _data[1]
                _ref_frame_from = _data[2]
                _coorsystem_from = _data[3]
                _ref_frame_to = _data[4]
                _coorsystem_to = _data[5]
                _file_header = _data[6]
                _email = _data[7]
                _utm_zone = _data[8]
                _gk_zone = _data[9]
                _language = _data[10]
                _username = _data[11]
                _height_from = _data[12]
                _height_to = _data[13]
                _latlong_format = _data[14]
                _to_epoch = _data[15]
                _from_epoch = _data[16]
                _file_format = _filename[_filename.find('.'):]
        if(res):
            #Flag file as "conversion started"
            with connection.cursor() as cursor:
                _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                update_status_stri = 'UPDATE fileconversions SET status = 5, updated = %s WHERE id = %s;'
                status_data = (_updated,str(_id))
                update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed, notes) VALUES (%s, 5, %s, %s);' 
                log_data = (str(_id),_updated, "File conversion started")
                cursor.execute(update_status_stri, status_data)
                cursor.execute(update_log_stri, log_data)
                connection.commit()


            #CCT will only work if line is ended with a newline
            #Make sure file ends with newline
            __newlineinput = ['tail', '-n', '1', _location + str(_id) + '/' + _filename,]
            _newlinecommand =  ['wc', '--lines']
            pnl = Popen(__newlineinput, stdout=PIPE)
            p2nl = Popen(_newlinecommand, stdin=pnl.stdout, stdout=PIPE)
            pnl.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
            _ends_with_newline = p2nl.communicate()[0]

            #If no newline, append newline
            if str(_ends_with_newline).find('1') == -1:
                with open(_location + str(_id) + '/' + _filename, 'a') as fout:
                    fout.write('\n')

            #Create backup
            with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + '/' + 'backup_' + _filename, 'w') as fback:
                for line in fin:
                    fback.write(line)

            #Replace , with .
            with fileinput.FileInput(_location + str(_id) + '/' + _filename, inplace=True) as file:
                for line in file:
                    print(line.replace(',','.'), end='')

                
            #is true if coordinates in file have name
            _has_name = True
            #Take values from file and write them to a latlong helper file
            if(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
                _has_name = latlongHelper(_id, _filename, _file_format, _file_header, _has_name,0)
            else:
                with open(_location + str(_id) + '/' + _filename, 'r') as fin:
                    for i, line in enumerate(fin):
                        if i >= int(_file_header) and line[0] == '%':
                            _file_header += 1
                        elif i >= int(_file_header) and line[0] != '%':
                            _check_format = line.split()
                            if(len(_check_format) == 3):
                                _has_name = False
                            else:
                                _has_name = True

            needHjorseyHelper = (_ref_frame_from == 'Hjörsey' and _ref_frame_to != 'ISN93')  or (_ref_frame_to == 'Hjörsey' and _ref_frame_from != 'ISN93')
            if(needHjorseyHelper):
                hjorseyHelper( _id, _filename, _ref_frame_from, _coorsystem_from, _file_header, _utm_zone, _gk_zone, _file_format, _height_from, _has_name)

            needISN_DRF_ISHHelper = (_ref_frame_from == 'ISN2016' and _ref_frame_to == 'ISN_DRF' and _height_from == 'h' and _height_to == 'MSL')
            if(needISN_DRF_ISHHelper):
                drfISHHelper(_id, _filename, _ref_frame_from, _coorsystem_from, _file_header, _file_format, _height_from, _has_name)
                _height_to = 'h'

            skiplines = ["-s",str(_file_header)]
            #If input file has a name for each coordinate, columns 2,3,4,5 are used for the input
            # + make sure we use helper file to keep input-ed coordinates on correct format for cct
            if(_has_name):
                if(needHjorseyHelper):
                    columns = ["-c","2,3,4,5"]
                    _input_file = 'out/ISN93LCC' + _file_format
                    skiplines = ["-s",'0']
                    _ref_frame_from = 'ISN93'
                    _coorsystem_from = 'LCC'
                elif(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
                    columns = ["-c","3,2,4,5"]
                    _input_file = 'out/latlonghelper' + _file_format
                    skiplines = ["-s",'0']
                else:
                    columns = ["-c","2,3,4,5"]
                    _input_file = _filename
            else:
                if(needHjorseyHelper):
                    columns = ["-c","1,2,3,4"]
                    _input_file = 'out/ISN93LCC' + _file_format
                    skiplines = ["-s",'0']
                    _ref_frame_from = 'ISN93'
                    _coorsystem_from = 'LCC'
                elif(_coorsystem_from == 'LatLong' or _coorsystem_from == 'LatLongCPH'):
                    columns = ["-c","2,1,3,4"]
                    _input_file = 'out/latlonghelper' + _file_format
                    skiplines = ["-s",'0']
                else:
                    columns = ["-c","1,2,3,4"]
                    _input_file = _filename

            outputfile = ['-o', _location + str(_id) + '/out/conversionresults' + _file_format]

            t = _file_location
            t = t[:t.rfind('/')] + '/cocodatiserver'

            with open(os.path.join(t, 'configs/config.json')) as f:
                _config = json.load(f)


            latlong_edge_finished = False
            #Handle when ISN2016 Latlong to ISN2016 Latlong
            if(_ref_frame_from == _ref_frame_to and _coorsystem_from == _coorsystem_to and _height_from == _height_to):
                #use latlong helper to use the latlonghelper file instead of doing full conversions
                latlong_edge_finished = True
                latlongHelper(_id, _filename, _file_format, _file_header, True,1)
            #Most Cases
            else:
                _command = _config['systems'][_ref_frame_from][_height_from][_coorsystem_from][_ref_frame_to][_height_to][_coorsystem_to]['command']

                # If converting to or from UTM, we must append the correct UTM zone
                if _coorsystem_from == 'UTM' or _coorsystem_to == 'UTM':
                    _command = utmHandler(_utm_zone, _command)

                # If converting to GK, we must find correct central meridian for GK (Transverse Mercator)
                if _coorsystem_from == 'GK' or _coorsystem_to == 'GK':
                    _command = gkHandler(_gk_zone, _command)

                if _ref_frame_from == 'ISN_DRF' and _ref_frame_to == 'ISN_DRF':
                    _command = epochHandler(_command, _from_epoch, _to_epoch)
                elif _ref_frame_from == 'ISN_DRF':
                    _command = epochHandler(_command, _from_epoch, '2016.5')
                elif _ref_frame_to == 'ISN_DRF':
                    _command = epochHandler(_command, '2016.5', _to_epoch)

                
                _command = _command[0:1] + skiplines + columns + outputfile + _command[1:] + [_location + str(_id) + '/' + _input_file]

                p1 = Popen(_command, stdout=PIPE)
                p1.communicate()
                
                #See if conversion was a success
                with open(_location + str(_id) + '/out/conversionresults' + _file_format, 'r') as f:
                    first_line = f.readline()
                    if(first_line[0] == '#'):
                        raise ValueError('Contents of file are not on acceptable format')

            if _coorsystem_to == 'LatLong' and _latlong_format != 'Decimal' or latlong_edge_finished:
                formatLatLong(_id, _latlong_format, _file_format,latlong_edge_finished)
            
            lang = 'configs/languages/' + _language + '.json'
            
            with open(os.path.join(t, lang)) as f:
                _languages = json.load(f)

            _xyzOrder = _config['systemsinfo'][_coorsystem_to]['isOrderXYZ']
            _x = _languages['coordnames'][_height_to][_coorsystem_to]['x']
            _y = _languages['coordnames'][_height_to][_coorsystem_to]['y']
            _z = _languages['coordnames'][_height_to][_coorsystem_to]['z']
            
            #Init return file by inserting correct values into header
            with open(os.path.join(t, 'templates/'+ _language + '/headertemplate.txt'), 'r') as f, open(_location + str(_id) + '/out/' + _filename, 'w') as fout:
                src = Template(f.read())
                if(_coorsystem_to == 'LatLong'):
                    d = {'x':_y, 'y':_x, 'z':_z}
                else:
                    d = {'x':_x, 'y':_y, 'z':_z}
                result = src.substitute(d)
                fout.write(result)

            if(needISN_DRF_ISHHelper):
                drfISHFormatter(_id,_coorsystem_to,_latlong_format,_filename, _file_format,_file_header,_has_name)
            else:
                if latlong_edge_finished:
                    ll_file = '/out/latlongformatter'

                    if _latlong_format == 'Decimal':
                        ll_file = '/out/conversionresults'

                    with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + ll_file + _file_format, 'r') as fconv:
                        with open(_location + str(_id) + '/out/' + _filename, 'a') as fout:  
                            for line in range(_file_header):
                                next(fin)
                            for x, y in zip(fin,fconv):
                                if(_has_name):
                                    x = x.split()[0]
                                    fout.write("{0}\t{1}".format(x, y))
                                else:
                                    #if(_latlong_format == 'Decimal'):
                                    #    print(y.split())
                                    fout.write("{0}".format(y))
                elif _coorsystem_to == 'LatLong' and _latlong_format != 'Decimal':
                    with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + '/out/latlongformatter' + _file_format, 'r') as fconv:
                        with open(_location + str(_id) + '/out/' + _filename, 'a') as fout:  
                            for line in range(_file_header):
                                next(fin)
                            for x, y in zip(fin,fconv):
                                x = x[:x.find('\t')]
                                if(_has_name):
                                    fout.write("{0}\t{1}\n".format(x, y))
                                else:
                                    fout.write("{0}\n".format(y))
                
                else:
                    #Append conversionresults to return file (same name as input file)
                    with open(_location + str(_id) + '/' + _filename, 'r') as fin, open(_location + str(_id) + '/out/conversionresults' + _file_format, 'r') as fconv:
                        with open(_location + str(_id) + '/out/' + _filename, 'a') as fout:  
                            for line in range(_file_header):
                                next(fin)
                            for x, y in zip(fin,fconv):
                                x = x[:x.find('\t')]
                                y = y[:y.rfind(' ')]
                                #If to latlong, switch x and y values so latitude comes before longtitue
                                if(_coorsystem_to == 'LatLong'):
                                    temp = y.split()
                                    if(len(temp) > 1):
                                        temp[1], temp[0] = temp[0],temp[1]
                                    y = "   ".join(str(x) for x in temp)
                                if(_has_name):
                                    fout.write("{0}\t{1}\n".format(x, y))
                                else:
                                    fout.write("{0}\n".format(y))
                        
            _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            update_status_stri = 'UPDATE fileconversions SET status = 2, updated = %s WHERE id = %s;'
            status_data = (_updated,str(_id))
            update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed) VALUES (%s, 2, %s);' 
            log_data = (str(_id),_updated)
            with connection.cursor() as cursor:
                cursor.execute(update_status_stri, status_data)
                cursor.execute(update_log_stri, log_data)
            sendEmail(_id, _filename,_username, _email, _language)
    except Exception as e:
        _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print('Error on ' + _updated + ' ' + str(e))

        update_status_stri = 'UPDATE fileconversions SET status = 1, updated = %s WHERE id = %s;'
        status_data = (_updated,str(_id))
        update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed) VALUES (%s, 1, %s);' 
        log_data = (str(_id),_updated)
        with connection.cursor() as cursor:
            cursor.execute(update_status_stri, status_data)
            cursor.execute(update_log_stri, log_data)
        sendFailedEmail(_id,_filename, _username, _email, _language)
        print('Email has been sent to alert of conversion failure')


main()