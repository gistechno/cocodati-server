#! /usr/bin/python3
# -*- coding: utf-8 -*-
#
import os
from django.conf import settings
from collections import namedtuple
from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from django.urls import reverse
from django.template import RequestContext
from django.http import JsonResponse
from rest_framework import routers
from rest_framework.response import Response
from rest_framework.decorators import action
import json
from django import forms
from rest_framework import viewsets, generics

import re

from subprocess import Popen,PIPE
import subprocess
import uuid
import ast
from datetime import datetime
from string import Template
import psycopg2


from django.db import connection


def index(request):
    """
    """
    return HttpResponse(" ")

global _location 

#Production
#_location = '/var/www/sandmerki/html/cocodati-files/'

#Development
_location = '/var/www/jardbakki/html/cocodati/'

def sendFailedEmail(_id,_filename, _username, _email, _language):
    with open(os.path.join(settings.PROJECT_ROOT, 'templates/'+ _language + '/fileerror.txt')) as f, open(_location + str(_id) + '/out/sendfile.txt', 'w') as fconv:
        src = Template(f.read())
        d = {'username':_username, 'id':_id, 'filename':_filename}
        result = src.substitute(d)
        fconv.write(result)

    _input = ['cat', _location + str(_id) + '/out/sendfile.txt']
    _command = ['./cocodati/send-mail.sh', '-f', 'lmi@lmi.is', '-s', 'Cocodati Conversions', '-v', _email]
    p1 = Popen(_input, stdout=PIPE)
    p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    p2.communicate()[0]


def sendEmail(_id,_filename, _username, _email, _language):

    t = os.path.abspath(os.path.dirname(__file__))
    t = t[:t.rfind('/')] + '/cocodati_server'

    with open(os.path.join(t, 'templates/'+ _language + '/sendfile.txt')) as f, open(_location + str(_id) + '/out/sendfile.txt', 'w') as fconv:
        src = Template(f.read())
        d = {'username':_username}
        result = src.substitute(d)
        fconv.write(result)

    _input = ['cat', _location + str(_id) + '/out/sendfile.txt']
    _command = ['./send-mail.sh', '-f', 'lmi@lmi.is', '-A', _location + str(_id) + '/out/' + _filename, '-s', 'Cocodati Conversions', '-v', _email]
    p1 = Popen(_input, stdout=PIPE)
    p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    p2.communicate()[0]

    _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    update_status_stri = 'UPDATE fileconversions SET status = 3, updated = %s WHERE id = %s;'
    status_data = (_updated,str(_id))
    update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed) VALUES (%s, 3, %s);' 
    log_data = (str(_id),_updated)
    
    with connection.cursor() as cursor:
        cursor.execute(update_status_stri, status_data)
        cursor.execute(update_log_stri, log_data)

def sendEmailReceived(_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT id, filename, ref_frame_from, coorsystem_from, ref_frame_to, coorsystem_to, email, username, language FROM fileconversions WHERE id = %s;', (str(_id),))
        res = cursor.fetchall()
    
    if(res):
        _data = res[0]
        #_id = _data[0]
        _filename = _data[1]
        _ref_frame_from = _data[2]
        _coorsystem_from = _data[3]
        _ref_frame_to = _data[4]
        _coorsystem_to = _data[5]
        _email = _data[6]
        _username = _data[7]
        _language = _data[8]
        _file_format = _filename[_filename.find('.'):]

        with open(os.path.join(settings.PROJECT_ROOT, 'templates/'+ _language + '/filereceived.txt')) as f, open(_location + str(_id) + '/out/filereceived.txt', 'w') as fconv:
            src = Template(f.read())
            d = {'username':_username, 'filename':_filename, 'reffrom':_ref_frame_from, 'coordfrom':_coorsystem_from, 'refto':_ref_frame_to, 'coordto':_coorsystem_to, 'id':_id}
            result = src.substitute(d)
            fconv.write(result)

        _input = ['cat', _location + str(_id) + '/out/filereceived.txt']
        _command = ['./cocodati/send-mail.sh', '-f', 'lmi@lmi.is', '-s', 'Cocodati Conversions', '-v', _email]
        p1 = Popen(_input, stdout=PIPE)
        p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
        #p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
        p2.communicate()[0]

class fileConvertViewset(viewsets.ModelViewSet):
    @action(methods=['post'],detail=True)
    def postfile(self,request, pk=None):
        _data = ast.literal_eval(request.data['body'])
        _id = uuid.uuid4()
        _file_name = request.data['uploadFile'].name
        _username = _data['username']
        _email = _data['email']
        _ref_from = _data['system']
        _coord_from = _data['from']
        _ref_to = _data['system_to']
        _coord_to = _data['to']
        _skiplines = _data['lines']
        _gk_zone = _data['gk_zone']
        _utm_zone = _data['utm_zone']
        _language = _data['language']
        _height_from = _data['height_from']
        _height_to = _data['height_to']
        _received = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        _latlong_format = _data['latlong_format']
        _to_epoch = _data['to_epoch']
        _from_epoch = _data['from_epoch']

        #Prevent spaces in filename to be an issue
        _file_name = "".join(_file_name.split())

        with open(os.path.join(settings.PROJECT_ROOT, 'configs/config.json')) as f:
            file_ = json.load(f)
        #If command is not found, return Error    
        try:
            if(_ref_from != 'Hjörsey' and _ref_to != 'Hjörsey') and (_ref_from != 'ISN2016' and _ref_to != "ISN_DRF" and _height_to != 'MSL'):
                _command = file_['systems'][_ref_from][_height_from][_coord_from][_ref_to][_height_to][_coord_to]['command']
        except:
            return Response({
                'status': 'Command not available'
            })

        #Build string for inserting information into fileconversions
        stri = 'INSERT INTO fileconversions(id,filename,email,ref_frame_from,coorsystem_from,ref_frame_to,coorsystem_to,file_header,received,status,utm_zone, gk_zone,username, language, height_from, height_to, latlong_format, to_epoch, from_epoch) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id;'
        stri_data = (str(_id), _file_name, _email, _ref_from, _coord_from, _ref_to, _coord_to, _skiplines, _received, '0', _utm_zone, _gk_zone, _username, _language, _height_from, _height_to, _latlong_format, str(_to_epoch), str(_from_epoch))
        
        log_stri = 'INSERT INTO status_log(uuid,status_id,changed) VALUES(%s,%s,%s) RETURNING id;'
        log_stri_data = (str(_id), '0', _received)
        
        #Insert into fileconversions
        with connection.cursor() as cursor:
            cursor.execute(stri,stri_data)
            cursor.execute(log_stri, log_stri_data)
            _uuid = cursor.fetchall()
        

        _command = ["mkdir", _location + str(_id)]
        p1 = Popen(_command, stdout=PIPE)
        p1.communicate()
        _command2 =['mkdir', _location + str(_id) + "/out"]
        p2 = Popen(_command2, stdout=PIPE)
        p2.communicate()

        with open(_location + str(_id) + '/' + _file_name, 'wb+') as destination:
            for chunk in request.data['uploadFile'].chunks():
                destination.write(chunk)

        p1 = Popen(['file', _location + str(_id) + '/' + _file_name],stdout=PIPE)
        output = str(p1.communicate()[0])

        if ((output.find('UTF-8')+1) or (output.find('ASCII')+1) or (output.find('UTF-16')+1)):
            sendEmailReceived(_id)
            return Response({
                'status': 'Success'
            })
        else:
            #Update database so file is labeled as flawed
            _updated = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            update_status_stri = 'UPDATE fileconversions SET status = 1, updated = %s WHERE id = %s;'
            status_data = (_updated,str(_id))
            update_log_stri = 'INSERT INTO status_log(uuid, status_id, changed, notes) VALUES (%s, 1, %s, %s);' 
            log_data = (str(_id),_updated, "File is not on acceptable format")
            
            with connection.cursor() as cursor:
                cursor.execute(update_status_stri, status_data)
                cursor.execute(update_log_stri, log_data)
            
            #User is alerted with a modal box instead of sending email
            sendFailedEmail(_id,_file_name, _username, _email, _language)

            return Response({
                'status': 'Bad File'
            })
        