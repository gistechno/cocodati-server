#! /bin/bash
#
#	2018-09-06 Hafliði Sigtryggur Magnússon
#
#	Send an e-mail with utf-8 message header
#
#	Usage:
#		send-mail.sh -A|--attachment attachment -s|--subject subject -v|--verbose -f|--from email to(s)
#
set -o errexit -o pipefail -o noclobber -o nounset

! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi
#
OPTIONS=A:s:vf:
LONGOPTS=attachment:,subject:,verbose,from:
#
# -use ! and PIPESTATUS to get exit code with errexit set
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

ATTACH=""
STEXT=""
FROM=""
VERBOSE=n

while true; do
    case "$1" in
        -A|--attachment)
            ATTACH="$2"
            shift 2
            ;;
        -f|--from)
            FROM="$2"
            shift 2
            ;;
        -v|--verbose)
            VERBOSE=y
            shift
            ;;
        -s|--subject)
            STEXT="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# handle non-option arguments
if [[ $# -ne 1 ]]; then
    echo "$0: A single e-mail to address is required."
    exit 4
fi
#
E_STEXT="=?utf-8?B?$(echo -n "${STEXT}"|base64 --wrap=0 --ignore-garbage)?="
IFS='<>' read -r -a from_adr <<<"$FROM"
email="${from_adr[0]}"
email_name=""
from_email="$email"
if [[ "${!from_adr[@]}" != "0" ]]; then
	email="${from_adr[1]}"
	email_name="${from_adr[0]}"
	e_email_name="=?utf-8?B?$(echo -n "${email_name}"|base64 --wrap=0 --ignore-garbage)?="
	from_email="${e_email_name} <${email}>"
fi
#
OPT="-a 'Content-Type: text/plain; charset=UTF-8'"
if [ -n "${ATTACH}" ]; then
	OPT="${OPT} -A '"
	OPT="${OPT}${ATTACH}'"
fi
OPT="${OPT} -a 'Subject: "
OPT="${OPT}${E_STEXT}'"
OPT="${OPT} -a 'From: "
OPT="${OPT}${from_email}'"
OPT="${OPT} -a 'Reply-To: "
OPT="${OPT}${from_email}'"
#OPT="${OPT} -a 'Return-Path: "
#OPT="${OPT}${from_email}'"
#
echo $@

eval mailx $OPT -r "$email" $@ <&0
