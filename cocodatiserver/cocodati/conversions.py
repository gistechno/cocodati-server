from collections import namedtuple
from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from django.urls import reverse
from django.template import RequestContext
from django.http import JsonResponse
from rest_framework import routers
from rest_framework.response import Response
from rest_framework.decorators import action
import json
from django import forms
from rest_framework import viewsets, generics
import os
import re
from django.conf import settings
from subprocess import Popen,PIPE
import subprocess
from django.db import connection

# Create your views here.

def index(request):
    """
    """
    return HttpResponse(" ")

def convert(_input, _command):  
    #_input = _input[:-1]
    #print(_input)
    print("hit convert")
    #print(_input)
    #print(_command)
    p1 = Popen(_input, stdout=PIPE)
    p2 = Popen(_command, stdin=p1.stdout, stdout=PIPE)
    p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    output = str(p2.communicate()[0])
    #print(output)
    return output

def getCommand(_system,_heightFrom,_from,_systemTo,_heightTo,_to):
    _command = ''
    get_command_stri = 'SELECT command from conversion_configs WHERE system_from = %s and height_from = %s and sub_from = %s and system_to = %s and height_to = %s and sub_to = %s;'
    get_command_data = (_system,_heightFrom,_from,_systemTo,_heightTo,_to)
    with connection.cursor() as cursor:
        cursor.execute(get_command_stri, get_command_data)
        res = cursor.fetchall()
        if(res):
            _command = res[0][0].split()
    return _command

def getValuesFromOutput(_output, _from, _system, _systemTo):
    temp = _output[2:]
    while temp.find(' ') == 0:
        temp = temp[1:]
    x = temp[:temp.find(' ')]

    temp = temp[temp.find(' '):]
    
    while temp.find(' ') == 0:
        temp = temp[1:]
    y = temp[:temp.find(' ')]

    
    temp = temp[temp.find(' '):]
    while temp.find(' ') == 0:
        temp = temp[1:]
    z = temp[:temp.find(' ')]
    
    return [y,x,z]

#Convert from 66d31'38.70748"(DMS) to 66.527418744 (decimal)
def LatlongToDecimal(_x,_y):
    #Convert 62°22'22" input to 62d22'22"
    if _y.find("W")+1:
        _x = _y[_y.find("N")+1:]
        _y = _y[:_y.find("N")]

    _x = _x.replace(" ", "")
    _y = _y.replace(" ", "")

    _x = _x.replace("″","\"")
    _y = _y.replace("″","\"")

    #if _x.find('′')+1:
    _x = _x.replace("′","\'")
    _y = _y.replace("′","\'")

    #print(_x)
    if _x.find('W')+1:
        _x = '-' + _x
        _x = _x.replace("W","")

    xd = float(_x[0:_x.find('d')])
    if xd < 0:
        xmultiplier = -1
    else:
        xmultiplier = 1

    if _x.find('\'')+1:
        xm = float(_x[_x.find('d')+1:_x.find('\'')])
    else:
        xm = 0
    
    if _x.find('\"')+1:
        xs = float(_x[_x.find('\'')+1:_x.find('\"')])
    else:
        xs = 0

    latlongX = xd + (xm * xmultiplier / 60) + (xs * xmultiplier / 3600)

    yd = float(_y[0:_y.find('d')])
    
    if yd < 0:
        ymultiplier = -1
    else:
        ymultiplier = 1

    if _y.find('\'')+1:
        ym = float(_y[_y.find('d')+1:_y.find('\'')])
    else:
        ym = 0
    
    if _y.find('\"')+1:
        ys = float(_y[_y.find('\'')+1:_y.find('\"')])
    else: 
        ys = 0

    latlongY = yd + (ym * ymultiplier / 60) + (ys * ymultiplier / 3600)

    
    return [latlongX, latlongY]

def utmHandler(_zone, _command):
    if _zone == '0':
        print("Error? No UTM zone given")
    else:
        if '+init=IS.txt:ISN_UTM28' in _command:
            utmIndex = _command.index('+init=IS.txt:ISN_UTM28')
            _command[utmIndex] = '+init=IS.txt:ISN_UTM' + _zone
            if '+init=IS.txt:ISN_UTM28' in _command:
                utmIndex2 = _command.index('+init=IS.txt:ISN_UTM28')
                if(utmIndex2 +1):
                    _command[utmIndex2] = '+init=IS.txt:ISN_UTM' + _zone
        
        if '+init=IS.txt:HJ_UTM28' in _command:
            hjorseyIndex = _command.index('+init=IS.txt:HJ_UTM28')
            if(hjorseyIndex + 1):
                _command[hjorseyIndex] = ("+init=IS.txt:HJ_UTM" + _zone)
            if '+init=IS.txt:HJ_UTM28' in _command:
                hjorseyIndex = _command.index('+init=IS.txt:HJ_UTM28')
                if(hjorseyIndex + 1):
                    _command[hjorseyIndex] = ("+init=IS.txt:HJ_UTM" + _zone)

        if '+init=IS.txt:RJ_UTM28' in _command:
            rvkIndex = _command.index('+init=IS.txt:RJ_UTM28')
            if(rvkIndex + 1):
                _command[rvkIndex] = ("+init=IS.txt:RJ_UTM" + _zone)

    return _command

def gkHandler(_gkZone, _command):
    if "+init=IS.txt:ISN_GK18" in _command:
        gkIndex = _command.index("+init=IS.txt:ISN_GK18")
        _command[gkIndex] = ("+init=IS.txt:ISN_GK" + _gkZone)
        if "+init=IS.txt:ISN_GK18" in _command:
            gkIndex2 = _command.index("+init=IS.txt:ISN_GK18")
            if(gkIndex2 + 1):
                _command[gkIndex2] = ("+init=IS.txt:ISN_GK" + _gkZone)

    if '+init=IS.txt:HJ_GK18' in _command:
        hjorseyIndex = _command.index('+init=IS.txt:HJ_GK18')
        if(hjorseyIndex + 1):
            _command[hjorseyIndex] = ("+init=IS.txt:HJ_GK" + _gkZone)
        if '+init=IS.txt:HJ_GK18' in _command:
            hjorseyIndex = _command.index('+init=IS.txt:HJ_GK18')
            if(hjorseyIndex + 1):
                _command[hjorseyIndex] = ("+init=IS.txt:HJ_GK" + _gkZone)

    if '+init=IS.txt:RJ_GK21' in _command:
        rvkIndex = _command.index('+init=IS.txt:RJ_GK21')
        if(rvkIndex + 1):
            _command[rvkIndex] = ("+init=IS.txt:RJ_GK" + _gkZone)

    return _command

def getLatLongXY(x, y, _x, _y, _zone, _gkZone, _input, _system, _from, _systemTo, _to, _heightFrom, _heightTo, _to_epoch, _from_epoch):
    # Need to make sure we also return latlong coordinates for pan-to feature in client
    # If converting to latlong, return converted coordinates

    if _to == 'LatLong':
        latlongX = float(x)
        latlongY = float(y)
    # If converting from latlong, return original coordinates
    elif _from == 'LatLong':
        # Check if coordinates are on form 66d31'38.70748" (DMS) or 66.527418744 (decimal)
        # If on form 66d31'38.70748" (DMS), we need to convert to 66.527418744 (decimal)
        isDegrees = _x.find('d') + 1
        if bool(isDegrees):
            temp = LatlongToDecimal(_x, _y)
            latlongX = temp[0]
            latlongY = temp[1]
        # Else return latlong on decimal form (66.527418744)
        else:
            latlongX = (float(_x))
            latlongY = (float(_y))
    # If neither _to or _from are latlong, we must convert to latlong
    elif _from == 'LatLongCPH':
        #convert from  _to to latlong
        #latlongCommand = file_['systems'][_system]['h']['LatLongCPH'][_system]['h']['LatLong']['command']
        latlongCommand = getCommand(_system,'h','LatLongCPH',_system,'h','LatLong')
        latlongCommand = utmHandler(_zone, latlongCommand)
        latlongCommand = gkHandler(_gkZone, latlongCommand)

        latlongInput = ['echo', _x, _y, '0','0']
        output = convert(latlongInput, latlongCommand)
        temp = getValuesFromOutput(output, _from, _system, _systemTo)
        y = temp[0]
        x = temp[1]

        latlongX = (float(x))
        latlongY = (float(y))
    elif _system == 'Reykjavík':
        #latlongCommand = file_['systems'][_system]['h'][_from][_systemTo]['h']['LatLong']['command']
        latlongCommand = getCommand(_system,'h',_from,_systemTo,'h','LatLong')
        latlongInput = ['echo', _x, _y, '0','0']
        output = convert(latlongInput, latlongCommand)
        temp = getValuesFromOutput(output, _from, _system, _systemTo)
        y = temp[0]
        x = temp[1]

        latlongX = (float(x))
        latlongY = (float(y))
    else:  
        #latlongCommand = file_['systems'][_system]['h'][_from][_system]['h']['LatLong']['command']
        latlongCommand = getCommand(_system,'h',_from,_system,'h','LatLong')
        latlongCommand = utmHandler(_zone, latlongCommand)
        latlongCommand = gkHandler(_gkZone, latlongCommand)
        #Make sure Epoch is correct
        if _system == 'ISN_DRF' and _systemTo == 'ISN_DRF':
            latlongCommand = epochHandler(latlongCommand, _from_epoch, _to_epoch)
        elif _system == 'ISN_DRF':
            latlongCommand = epochHandler(latlongCommand, _from_epoch, '2016.5')
        elif _systemTo == 'ISN_DRF':
            latlongCommand = epochHandler(latlongCommand, '2016.5', _to_epoch)

        print("latlong command error")
        output = convert(_input, latlongCommand)

        temp = output[2:]

        while temp.find(' ') == 0:
            temp = temp[1:]
        latlongX = float(temp[:temp.find(' ')])

        temp = temp[temp.find(' '):]
        
        while temp.find(' ') == 0:
            temp = temp[1:]
        latlongY = float(temp[:temp.find(' ')])
    return [latlongX, latlongY]



def hjorseyHandler(_x, _y, _z, _input, _system, _from, _systemTo, _to, _heightFrom, _heightTo, _zone, _gkZone):
    try:
        #_command = file_['systems'][_system]['h'][_from]['ISN93']['h']['LCC']['command']
        _command = getCommand(_system,'h',_from,'ISN93','h','LCC')

    except:
        print('Error: Transformation from:'+ _system + ' ' + _from + ' ' + _heightFrom + ' TO: ' +  _systemTo + ' ' + 'LCC' + ' ' + _heightTo + ' is not installed')
        return Response({
            'y':'Error',
            'x':'Conversion not installed',
            'z':'Try another conversion',
            'latlongX': -19,
            'latlongY': 65
        })

    if(_from == 'UTM'):
        _command = utmHandler(_zone, _command)
    elif (_from == 'GK'):
        _command = gkHandler(_gkZone, _command)

    output = convert(_input, _command)

    #Get values on correct form from output string
    temp = getValuesFromOutput(output, _from, _system, _systemTo)
    y = temp[0]
    x = temp[1]
    z = temp[2]
    
    return [x,y,z]

def formatLatLong(x,y,formatId):
    if formatId == 'Decimal':
        return [x,y]
    else:
        if(x.find('.') == -1):
            x = x + '.0'
        if(y.find('.') == -1):
            y = y + '.0'
        xd = (x[:x.find('.')])
        xm = float('0' + x[x.find('.'):]) * 60
        yd = (y[:y.find('.')])
        ym = float('0' + y[y.find('.'):]) * 60
        if formatId == 'DM':
            xm = str(round(xm,6))
            ym = str(round(ym,6))
            x = xd + '°' + xm + '\''
            y = yd + '°' + ym + '\''
        elif formatId == 'DMS':
            xm = str(xm)
            ym = str(ym)
            xs = float('0' + xm[xm.find('.'):]) * 60
            xs = round(xs, 6)
            xs = str(xs)
            ys = float('0' + ym[ym.find('.'):]) * 60
            ys = round(ys, 6)
            ys = str(ys)
            xm = (xm[:xm.find('.')])
            ym = (ym[:ym.find('.')])
            x = xd + '°' + xm + '\'' + xs + '\"'
            y = yd + '°' + ym + '\'' + ys + '\"'   
    return [x,y]


def epochHandler(_command, _from_epoch, _to_epoch):
    print("hit epochhandler")

    diff = float(_to_epoch) - float(_from_epoch)
    if "+t_epoch=2017.5" in _command:
        index = _command.index("+t_epoch=2017.5")
        _command[index] = ("+dt=" + str(diff))
    elif "+t_epoch=2016.5" in _command:
        index = _command.index("+t_epoch=2016.5")
        _command[index] = ("+dt=" + str(diff))

    if "+t_obs=2017.5" in _command:
        _command.remove("+t_obs=2017.5")
    elif "+t_obs=2016.5" in _command:
        _command.remove("+t_obs=2016.5")

    return _command


def drfHandler(_command, _from, _to, _from_epoch, _to_epoch):
    if _from == 'LCC':
        if _from_epoch == "2016.5":
            if "+init=IS.txt:KIN_LAM" in _command:
                index = _command.index("+init=IS.txt:KIN_LAM")
                _command[index] = ("+init=IS.txt:ISN2016_LAM")
    if _to == 'LCC':
        if _to_epoch == "2016.5":
            index = len(_command) - 1
            _command[index] = "+init=IS.txt:ISN2016_LAM"

    return _command

def drfISHHandler(_x, _y, _z, _input, _system, _from, _systemTo, _to, _heightFrom, _heightTo, _from_epoch,_to_epoch):
    try:
        #_command = file_['systems'][_system]['h'][_from]['ISN_DRF']['h'][_to]['command']
        _command = getCommand(_system,'h',_from,'ISN_DRF','h',_to)

    except:
        print('Error: Transformation from:'+ _system + ' ' + _from + ' ' + _heightFrom + ' TO: ' +  _systemTo + ' ' + _to + ' ' + _heightTo + ' is not installed')
        return Response({
            'y':'Error',
            'x':'Conversion not installed',
            'z':'Try another conversion',
            'latlongX': -19,
            'latlongY': 65
        })

    if _system == 'ISN_DRF' and _systemTo == 'ISN_DRF':
        _command = epochHandler(_command, _from_epoch, _to_epoch)
    elif _system == 'ISN_DRF':
        _command = epochHandler(_command, _from_epoch, '2016.5')
    elif _systemTo == 'ISN_DRF':
        _command = epochHandler(_command, '2016.5', _to_epoch)

    output = convert(_input, _command)

    #Get values on correct form from output string
    temp = getValuesFromOutput(output, _from, _system, _systemTo)
    y = temp[0]
    x = temp[1]
    z = temp[2]
    
    return [x,y,z]


class convertViewSet(viewsets.ModelViewSet):
    """
    Main convert function
    Called with http request to: /cocodati/api/convert/1/convertCoords/

    Request received will be on form:
    [{
      system: (ex: ISN93),
      system_to: (ex: ISN93),
      from: (ex: latlong),
      to: (ex: lcc),
      y: (ex: 66d31'38.70748" or 66.527418744),
      x: (ex: -17d58'53.81890" or -17.981616361),
      z: (ex: 75.618 or empty string),
    }];
    """
    @action(methods=['post'],detail=True)
    def convertCoords(self, request, pk=None):
        #Get data from request
        data = request.data[0]
        _system = data['system']
        _systemTo = data['system_to']
        _from = data['from']
        _to = data['to']
        _x = data['x']
        _y = data['y']
        _z = data['z']
        _zone = data['zone']
        _latlongFormat = data['latlong_format']
        _heightFrom = data['height_from']
        _heightTo = data['height_to']
        _gkZone = data['gk_zone']  
        _to_epoch = data['to_epoch']
        _from_epoch = data['from_epoch']

        
        print('Converting from: '+ _system + ' ' + _from + ' ' + _heightFrom)
        print('Converting to: '+ _systemTo + ' ' + _to + ' ' + _heightTo)
        print('With values: '+ _x + ' ' + _y + ' ' + _z) 
        if _from == 'XYZ':
            _heightFrom = 'h'
        if _to == 'XYZ':
            _heightTo = 'h'

        #print(_system + ' ' + _from + ' ' + _heightFrom + ' -> ' + _systemTo + ' ' + _to + ' ' + _heightTo)
        _x = _x.replace(",", ".")
        _y = _y.replace(",", ".")
        _z = _z.replace(",", ".")

        #Convert 62 22 22 input to 62d22'22"
        if _from == 'LatLong' and _x.find(' ')+1 and not _x.find('°')+1:
            _x = _x.replace(" ", 'd',1)
            if _x.find(' ')+1:
                _x = _x.replace(" ", "\'", 1)
                _x = _x + "\""
            elif _x.find('.')+1:
                degtemp = _x[_x.find('d')+1:]
                fl = float(degtemp) / 60
                fl = str(fl)
                _x = _x[:_x.find('d')] + fl[fl.find('.'):]

        if _from == 'LatLong' and _y.find(' ')+1 and not _y.find('°')+1:
            _y = _y.replace(" ", 'd', 1)
            if _y.find(' ')+1:
                _y = _y.replace(" ", "\'", 1)
                _y = _y + "\""
            elif _y.find('.')+1:
                degtemp = _y[_y.find('d')+1:]
                fl = float(degtemp) / 60
                fl = str(fl)
                _y = _y[:_y.find('d')] + fl[fl.find('.'):]
    
        #Convert 62°22'22" input to 62d22'22"
        if _from == 'LatLong' and _x.find('°'):
            _x = _x.replace("°","d")
        if _from == 'LatLong' and _y.find('°'):
            _y = _y.replace("°","d")
        isDegrees = _y.find('d') + 1
        isDegreesX = _x.find('d') + 1

        #If true, input is latlong on form DMS. We need to convert to decimal before converting.  Because cct handles decimal better than dms
        if bool(isDegrees) or bool(isDegreesX):
            temp = LatlongToDecimal(_x, _y)
            #Put the decimal values in _input
            _x = str(temp[0])
            _y = str(temp[1])
        
        _input_x = _x
        _input_y = _y
        _input_z = _z
        _input_to = _to

        #with open(os.path.join(settings.PROJECT_ROOT, 'configs/config.json')) as f:
        #    file_ = json.load(f)

        _input = ['echo', _x, _y,_z,'0']

        if(_from == _to and _system == _systemTo and _heightFrom == _heightTo and _system != 'ISN_DRF'):
            if _from != 'LatLong':
                temp = getLatLongXY(_x, _y, _x, _y, _zone,_gkZone, _input, _system, _from, _systemTo, _to ,_heightFrom, _heightTo, _to_epoch, _from_epoch)
                _latlongX = temp[0]
                _latlongY = temp[1]

            if _from == 'LatLong':
                _latlongX = _x
                _latlongY = _y
                temp = formatLatLong(_x,_y,_latlongFormat)
                _x = temp[0]
                _y = temp[1]
                
            return Response({
                'y':_y,
                'x':_x,
                'z':_z,
                'latlongX': float(_latlongX),
                'latlongY': float(_latlongY)
            })

        if(_system == 'Hjörsey' and _systemTo != 'ISN93' and _systemTo != 'Reykjavik1900') or (_systemTo == 'Hjörsey' and _system != 'ISN93' and _system != 'Reykjavik1900'):
            temp = hjorseyHandler(_x, _y, _z, _input, _system, _from, _systemTo, _to, _heightFrom, _heightTo, _zone, _gkZone)
            _x = temp[0]
            _y = temp[1]
            _z = temp[2]
            _input = ['echo', _x, _y,_z,'0']
            _system = 'ISN93'
            _from = 'LCC'


        #See explanation in readme
        #TODO make readme
        twoStep = False
        if(_systemTo == 'ISN_DRF' and _heightTo == 'MSL' and (_from_epoch != '2016.5' or _to_epoch != '2016.5')):
            twoStep = True
            temp = drfISHHandler(_x, _y, _z, _input, _system, _from, _systemTo, _to, _heightFrom, _heightTo, _from_epoch,_to_epoch)
            _x = temp[0]
            _y = temp[1]
            _z = temp[2]
            if(_system == 'ISN_DRF'):
                _input = ['echo',_x,_y,_z,'0']
                _system =  _systemTo
                _from = _to
                _from_epoch = _to_epoch
                _to_epoch = '2016.5'
                _to = 'LatLong'
            elif(_system == 'ISN2016'):
                _systemTo = 'ISN2016'


        #Because of proj4 update
        # 15/12/2019 New proj update makes this not
        #if(_system == 'ISN_DRF'):
        #    _input[4] = _from_epoch


        try:
            #_command = file_['systems'][_system][_heightFrom][_from][_systemTo][_heightTo][_to]['command']
            
            _command = getCommand(_system,_heightFrom,_from,_systemTo,_heightTo,_to)
        except:
            print('Error: Transformation from:'+ _system + ' ' + _from + ' ' + _heightFrom + ' TO: ' +  _systemTo + ' ' + _to + ' ' + _heightTo + ' is not installed')
            return Response({
                'y':'Error',
                'x':'Conversion not installed',
                'z':'Try another conversion',
                'latlongX': -19,
                'latlongY': 65
            })

        
        # If converting to or from UTM, we must append the correct UTM zone
        if _from == 'UTM' or _to == 'UTM':
            _command = utmHandler(_zone, _command)

        # If converting to GK, we must find correct central meridian for GK (Transverse Mercator)
        if _from == 'GK' or _to == 'GK':
            _command = gkHandler(_gkZone, _command)

        # If converting ISN_DRF, we must add correct epoch
        if _system == 'ISN_DRF' and _systemTo == 'ISN_DRF':
            _command = epochHandler(_command, _from_epoch, _to_epoch)
        elif _system == 'ISN_DRF':
            _command = epochHandler(_command, _from_epoch, '2016.5')
        elif _systemTo == 'ISN_DRF':
            _command = epochHandler(_command, '2016.5', _to_epoch)


        #Changed: will not change +init=IS.txt:KIN_LAM  to +init=IS.txt:ISN2016_LAM for epoch 2016.5
        #if _systemTo == 'ISN_DRF' and _system == 'ISN_DRF':
        #    _command = drfHandler(_command, _from, _to, _from_epoch, _to_epoch)
            
        
        # Convert
        try:
            #print(_command)
            output = convert(_input, _command)
            #print(output)
            #Get values on correct form from output string
            temp = getValuesFromOutput(output, _from, _system, _systemTo)
            y = temp[0]
            x = temp[1]
            z = temp[2]

            
            temp = getLatLongXY(x, y, _input_x, _input_y, _zone, _gkZone, _input, _system, _from, _systemTo, _to ,_heightFrom, _heightTo, _to_epoch, _from_epoch)
            latlongX = temp[0]
            latlongY = temp[1]

            #If two parted conversion, return x and y values from first part
            if(twoStep):
                x = _x
                y = _y


            #Return LatLong on correct format, Decimal, DMS or DM
            if _input_to == 'LatLong':
                temp = formatLatLong(x,y,_latlongFormat)
                x = temp[0]
                y = temp[1]
            
        except:
            return Response({
                'y':'Please check values',
                'x':'Conversion Error',
                'z':'',
                'latlongX': -19,
                'latlongY': 65
            })

        # Return converted coordinates and the latlong conversions        
        return Response({
            'y':y,
            'x':x,
            'z':z,
            'latlongX': latlongX,
            'latlongY': latlongY
        })
        