from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework import viewsets
import os
from django.conf import settings
# Create your views here.

def index(request):
    """
    """
    return HttpResponse(" ")


class getConfigViewSet(viewsets.ModelViewSet):
    """
    Returns config file (config.json) with http request to:
    /cocodati/api/config/1/getconfig/
    """
    @action(methods=['get'],detail=True)
    def getconfig(self,request, pk=None):
        with open(os.path.join(settings.PROJECT_ROOT, 'configs/config.json')) as f:
            file_ = f.read()

        return HttpResponse(file_, content_type="text/json")

    @action(methods=['get'],detail=True)
    def gettestcases(self,request, pk=None):
        with open(os.path.join(settings.PROJECT_ROOT, 'configs/testcases.json')) as f:
            file_ = f.read()

        return HttpResponse(file_, content_type="text/json")

    @action(methods=['get'],detail=True)
    def getwmts(self, request, pk=None):
        with open(os.path.join(settings.PROJECT_ROOT, 'configs/wmts.xml')) as f:
            file_ = f.read()
    
        return HttpResponse(file_, content_type="text/xml")

    @action(methods=['get'],detail=True)
    def getwfs(self, request, pk=None):
        with open(os.path.join(settings.PROJECT_ROOT, 'configs/wfs.xml')) as f:
            file_ = f.read()
    
        return HttpResponse(file_, content_type="text/xml")

    @action(methods=['get'],detail=True)
    def getlanguages(self, request, pk=None):
        print(pk)
        if(pk == '0'):
            language = 'icelandic'
        else:
            language = 'english'

        with open(os.path.join(settings.PROJECT_ROOT, 'configs/languages/' + language +'.json')) as f:
            file_ = f.read()
    
        return HttpResponse(file_, content_type="text/json")